==================
Printing documents
==================

Yet another lack in the Developer Guide is some introduction to the
question: how to produce printable documents from your Lino application.



:mod:`lino.mixins.printable`
:mod:`lino.modlib.excerpts`.
