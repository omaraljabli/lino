"""
.. toctree::

.. autosummary::
   :toctree:

    diag
    initdb
    initdb_demo
    makedocs
    makescreenshots
    mergedata
    monitor
    run
    show
    syncscreenshots



"""
