============================
The model library ``modlib``
============================

This document is being migrated to :mod:`lino.modlib`.

.. module:: ml

.. toctree::
   :maxdepth: 2

   extensible
   households
   humanlinks
   ledger


.. toctree::
   :hidden:

   debts
   contacts
   addresses
   beid
   boards
   cal
   countries
   cv
   excerpts
   reception
   users
   courses
   extjs
   sales
   vat
   lino
   polls
