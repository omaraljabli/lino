.. _lino.projects:

==============================
Ready-to-use Lino applications
==============================

Included with Lino
==================

Lino comes with a collection of example applications used by the test
suite and for learning:

- :mod:`lino.projects`


Separate code repositories
==========================

The following Lino applications are available as separate projects
maintained by the same author:

- `Lino Faggio <http://faggio.lino-framework.org/>`_
- `Lino Patrols <http://patrols.lino-framework.org/>`_
- `Lino Così <http://cosi.lino-framework.org/>`_
- `Lino Logos <http://logos.lino-framework.org/>`_

.. _welfare:

Lino Welfare
------------

http://welfare.lino-framework.org/

.. _cosi:

Lino Così 
------------

http://cosi.lino-framework.org/

.. _faggio:

Lino Faggio
------------

http://faggio.lino-framework.org/

.. _patrols:

Lino Patrols
------------

http://patrols.lino-framework.org/

.. _logos:

Lino Logos
------------

http://logos.lino-framework.org/

.. _sunto:

Lino Sunto
----------

Lino Sunto is the first free (GPL) Lino application developed by
somebody else than the author. It is hosted at    
https://github.com/ManuelWeidmann/lino-sunto




