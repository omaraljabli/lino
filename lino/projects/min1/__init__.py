"""A minimal application used by a number of tests and tutorials. It
is not meant to be actually useful in the real world.

It uses the following plugins:

- 
- :mod:`lino.modlib.system`
- :mod:`lino.modlib.users`
- :mod:`lino.modlib.contacts` (which needs :mod:`lino.modlib.countries`)
- :mod:`lino.modlib.cal` (which needs :mod:`lino.modlib.contenttypes`)
- :mod:`lino.modlib.export_excel`

.. autosummary::
   :toctree:

   settings


"""
