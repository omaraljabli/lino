# -*- coding: UTF-8 -*-
# Copyright 2012-2015 Luc Saffre
# License: BSD (see file COPYING for details)

r"""Utilities for `lino.modlib.accounts`.


"""

from decimal import Decimal
ZERO = Decimal(0)

DEBIT = True
CREDIT = False
