======
Ledger
======

.. module:: ml.ledger


Content moved to :mod:`lino.modlib.ledger`.



Debts
-----


.. class:: DebtsByAccount

.. class:: DebtsByPartner


Account balances
----------------

.. class:: AccountsBalance


.. class:: GeneralAccountsBalance

.. class:: PartnerAccountsBalance

.. class:: ClientAccountsBalance

.. class:: SupplierAccountsBalance




.. class:: DebtorsCreditors

.. class:: Debtors

.. class:: Creditors



Reports
-------

.. class:: Situation

.. class:: ActivityReport

