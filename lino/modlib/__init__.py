# Copyright 2008-2015 Luc Saffre
# License: BSD (see file COPYING for details)

"""The standard model library included with Lino.

The modules in the :mod:`lino.modlib` package are ready-to-use "apps"
(as Django calls them) for Lino applications.

"System apps"
=============

.. autosummary::
   :toctree:

    bootstrap3
    contenttypes
    excerpts
    extjs
    lino
    plausibility
    users
    system

"Options"
=========

.. autosummary::
   :toctree:

    about
    changes
    export_excel
    extensible
    tinymce
    stars

"Enterprise Resources"
======================

.. autosummary::
   :toctree:

    accounts
    addresses
    appypod
    awesomeuploader
    beid
    blogs
    boards
    cal
    comments
    concepts
    contacts
    countries
    courses
    cv
    davlink
    declarations
    dupable_partners
    eid_jslib
    events
    families
    finan
    households
    humanlinks
    iban
    importfilters
    languages
    ledger
    lists
    notes
    office
    orders
    outbox
    pages
    polls
    postings
    print_pisa
    products
    projects
    properties
    reception
    rooms
    sales
    sepa
    smtpd
    thirds
    clocking
    tickets
    uploads
    vat
    vocbook
    workflows


Differentiator packages
=======================

.. autosummary::
   :toctree:

    auto
    statbel

"""
