Designing data migrations
=========================

Lino includes a system for managing database migrations which takes a
radically different approach thanDjango's default `Migrations
<https://docs.djangoproject.com/en/dev/topics/migrations/>`_ system.

This system is based on :doc:`Python serializer </topics/dpy>`.
