Local system administrator
==========================

A **local system administrator** is the person who installs some Lino
application in order to make it available on a given web server.
